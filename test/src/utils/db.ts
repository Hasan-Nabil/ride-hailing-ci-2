import { Sequelize, Model, DataTypes, QueryTypes } from 'sequelize';
import * as JsonSQL from 'json-sql';

const db = new Sequelize({
  database: process.env["POSTGRES_DATABASE"] || "ridehailing",
  username: process.env["POSTGRES_USERNAME"] || "postgres",
  password: process.env["POSTGRES_PASSWORD"] || "postgres",
  host: process.env["POSTGRES_HOST"] || "localhost",
  port: parseInt(process.env["POSTGRES_PORT"], 10) || 5432,
  dialect: "postgres",
  logging: false,
  timezone: "Asia/Jakarta"
});

interface SelectQuery {
  type: 'select';
  table: string;
  distinct?: boolean;
  fields?: Array<any> | any;
  alias?: string | any;
  join?: Array<any> | any;
  condition?: Array<any> | any;
  group?: string | Array<any>;
  sort?: string | Array<any> | any;
  limit?: number;
  offset?: number;
}

interface InsertQuery {
  type: 'insert';
  table: string;
  values: Array<any> | any;
  with?: Array<any> | any;
  withRecursive?: Array<any> | any;
  or?: string;
  condition?: Array<any> | any;
  returning?: Array<any> | any;
}

interface UpdateQuery {
  type: 'update';
  table: string;
  modifier: any | {
    $set?: any;
    $inc?: any;
    $dec?: any;
    $mul?: any;
    $div?: any;
    $default?: any;
  };
  or?: string;
  condition?: Array<any> | any;
  returning?: Array<any> | any;
}

interface RemoveQuery {
  type: 'remove';
  table: string;
  with?: Array<any> | any;
  withRecursive?: Array<any> | any;
  condition?: Array<any> | any;
  returning?: Array<any> | any;
}

interface CombinationQuery {
  type: 'union' | 'intersect' | 'except';
  all?: boolean;
  with?: Array<any> | any;
  withRecursive?: Array<any> | any;
  queries: Array<SelectQuery>;
  sort?: string | Array<any> | any;
  limit?: number;
  offset?: number;
}

type Query = SelectQuery | InsertQuery | UpdateQuery | RemoveQuery | CombinationQuery;

const jsonSql = JsonSQL({
  dialect: 'postgresql'
});

/**
 * Perform database query
 *
 * @export
 * @param {Query} queryObject query definition in json object
 * @param {boolean} [multiple=false] true affected rows is multiple (only for delete & update)
 * @returns query result in object
 */
export function dbQuery(
  queryObject: Query, multiple: boolean = false
) {
  let type: string;
  switch (queryObject.type) {
    case 'select':
      type = QueryTypes.SELECT;
      break;
    case 'insert':
      type = QueryTypes.INSERT;
      break;
    case 'update':
      type = multiple ? QueryTypes.BULKUPDATE : QueryTypes.UPDATE;
      break;
    case 'remove':
      type = multiple ? QueryTypes.BULKDELETE : QueryTypes.DELETE;
      break;
    case 'union':
    case 'intersect':
    case 'except':
      type = QueryTypes.SELECT;
      break;
  }

  // make query
  const { query, values } = jsonSql.build(queryObject);
  return db.query(query, { bind: values, type });
}

export class TrackEvent extends Model {}
TrackEvent.init(
  {
    rider_id: DataTypes.INTEGER,
    north: DataTypes.FLOAT,
    west: DataTypes.FLOAT,
    east: DataTypes.FLOAT,
    south: DataTypes.FLOAT
  },
  { modelName: "track_events", sequelize: db }
);
export class DriverPosition extends Model {}
DriverPosition.init(
  {
    rider_id: DataTypes.INTEGER,
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT
  },
  { modelName: "driver_positions", sequelize: db }
);

export function syncDB(): Promise<Sequelize> {
  return db.sync();
}
